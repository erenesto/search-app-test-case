import { ProductList, Trends } from 'components'
import Header from 'components/header'
import Head from 'next/head'
import React from 'react'

const HomePage = ({ products }) => {
  return (
    <>
      <Head>
        <title>Matspar | Product Search</title>
      </Head>
      <div className="wrapper">
        <Header />
        <ProductList products={products} />
      </div>
    </>
  )
}

export async function getServerSideProps(context) {
  const query = context.query.query || ''
  const body = {
    slug: '/kategori',
    query: {
      q: query
    }
  }
  try {
    const data = await fetch(`https://api.matspar.se/slug`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    })

    const { payload } = await data.json()
    return {
      props: {
        products: payload.products
      }
    }
  } catch (error) {
    console.log(error)
    return {
      props: {
        products: []
      }
    }
  }
}

export default HomePage
