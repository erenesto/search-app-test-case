export default async function handler(req, res) {
  try {
    const response = await fetch(`https://api.matspar.se/autocomplete?query=${req.query.query}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })

    const data = await response.json()

    return res.status(200).json(data)
  } catch (error) {
    console.log(error)
    return res.status(500).json({ message: error.message })
  }
}
