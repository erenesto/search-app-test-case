import { Card } from 'components/ui'
import React from 'react'
import styles from './index.module.css'
import Image from 'next/image'
import utils from 'utils'

const ProductCard = ({ product }) => {
  return (
    <Card>
      <div className={styles.product}>
        <Image
          src={utils.getProductImageUrl(product?.image) || '/images/placeholder.png'}
          alt="Product"
          width={141}
          height={101}
          layout="intrinsic"
          objectFit="contain"
        />
        <div className={styles.info}>
          <h3 className={styles.name}>{product.name}</h3>
          <h4 className={styles.brand}>{product.brand}</h4>
          <div className={styles.price}>{utils.formatCurrency(product.price)}</div>
        </div>
      </div>
    </Card>
  )
}

export default ProductCard
