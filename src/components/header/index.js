import { Menu, Search } from 'components/icons'
import SearchBar from 'components/search-bar'
import React from 'react'
import styles from './index.module.css'

const Header = () => {
  return (
    <header className={styles.wrapper}>
      <Menu width={24} height={24} />
      <SearchBar />
    </header>
  )
}

export default Header
