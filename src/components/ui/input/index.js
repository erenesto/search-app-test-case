import React from 'react'
import styles from './index.module.css'
import PropTypes from 'prop-types'

const Input = ({ type, placeholder, icon, ...props }) => {
  return (
    <div className={styles.wrapper}>
      {icon && <span className={styles.icon}>{icon}</span>}
      <input type={type} className={styles.input} placeholder={placeholder} {...props} />
    </div>
  )
}

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  icon: ''
}

Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  icon: PropTypes.element,
  handleSubmit: PropTypes.func
}

export default Input
