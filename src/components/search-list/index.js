import { Close, Search } from 'components/icons'
import React from 'react'
import styles from './index.module.css'
import PropTypes from 'prop-types'
import utils from 'utils'

const SearchList = ({
  items,
  type,
  focused,
  onClickItem,
  handleKeyDown,
  setRecent,
  recentLength
}) => {
  const title = type === 'recent' ? 'Recent searches' : 'Popular searches'

  const renderHeader = () => {
    if (type === 'recent') {
      return (
        <>
          <h4 className={styles.title}>{title}</h4>
          <button
            className={styles.clear}
            onClick={() => {
              utils.clearRecentSearches()
              setRecent([])
            }}>
            Clear all
          </button>
        </>
      )
    }

    if (type === 'popular') {
      return <h4 className={styles.title}>{title}</h4>
    }

    return null
  }

  return (
    <div className={styles.searchList}>
      <div className={styles.header}>{renderHeader()}</div>

      <ul className={`${styles.list} ${type === 'recent' && styles.recent}`}>
        {items?.map((item, i) => {
          let index = i
          if (recentLength > 0 && type === 'popular') {
            index = i + recentLength
          }
          return (
            <li
              key={index}
              className={styles.item}
              role="option"
              style={{
                fontWeight: index === focused ? 'bold' : '',
                cursor: type === 'popular' ? 'pointer' : 'default'
              }}
              aria-selected={focused === index}>
              <span
                tabIndex={index}
                onClick={onClickItem}
                onKeyDown={handleKeyDown}
                style={{
                  flex: 1,
                  cursor: 'pointer'
                }}>
                {item}
              </span>

              {type === 'recent' && (
                <Close
                  width={15}
                  height={15}
                  onClick={() => {
                    utils.removeItemFromRecentSearches(item)
                    setRecent(utils.getRecentSearches())
                  }}
                  style={{ cursor: 'pointer' }}
                />
              )}
              {type === 'popular' && <Search width={15} height={15} />}
            </li>
          )
        })}
      </ul>
    </div>
  )
}

SearchList.propTypes = {
  type: PropTypes.oneOf(['recent', 'popular'])
}

export default SearchList
