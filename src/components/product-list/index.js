import ProductCard from 'components/product-card'
import Trends from 'components/trends'
import React from 'react'
import styles from './index.module.css'
import PropTypes from 'prop-types'

const ProductList = ({ products }) => {
  return (
    <div className={styles.wrapper}>
      <h1 className="headline">Find your favorite products now.</h1>

      <Trends />

      <div className={styles.products}>
        {products.length === 0 && (
          <div className={styles.empty}>
            <h3>No products found</h3>
            <p>Try to search for something else.</p>
          </div>
        )}
        {products.map((product, index) => (
          <ProductCard key={index} product={product} />
        ))}
      </div>
    </div>
  )
}

ProductList.defaultProps = {
  products: []
}

ProductList.propTypes = {
  products: PropTypes.array
}

export default ProductList
