import { useRouter } from 'next/router'
import React from 'react'
import styles from './index.module.css'

const trends = ['Bread', 'Milk', 'Egg']

const Trends = () => {
  const router = useRouter()

  const handleTrendClick = (val) => {
    router.push(`?query=${val.toLowerCase()}`)
  }

  return (
    <div className={styles.wrapper}>
      <h2 className={styles.title}>Trendy foods</h2>
      <ul className={styles.list}>
        {trends.map((trend, i) => (
          <li key={i} className={styles.item} onClick={() => handleTrendClick(trend)}>
            {trend}
          </li>
        ))}
      </ul>
    </div>
  )
}

export default Trends
