import { Search } from 'components/icons'
import { Input } from 'components/ui'
import React from 'react'
import styles from './index.module.css'
import cn from 'classnames'
import SearchList from 'components/search-list'
import debounce from 'lodash.debounce'
import { useRouter } from 'next/router'
import utils from 'utils'

const SearchBar = () => {
  const router = useRouter()
  const [isActive, setIsActive] = React.useState(false)
  const [searchValue, setSearchValue] = React.useState('')
  const [list, setList] = React.useState([])
  const [recent, setRecent] = React.useState([])
  const [allItems, setAllItems] = React.useState([])
  const [focused, setFocused] = React.useState(-1)

  const getAutocomplete = async (val = '') => {
    const response = await fetch(`/api/autocomplete?query=${val}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    const data = await response.json()
    const values = data.suggestions.map((item) => item.text)
    setList(values)
  }

  React.useEffect(() => {
    getAutocomplete()
    setRecent(utils.getRecentSearches())
  }, [])

  React.useEffect(() => {
    setAllItems([...recent, ...list])
  }, [recent, list])

  const addRecent = (val) => {
    utils.addToRecentSearches(val)
    setRecent(utils.getRecentSearches())
  }

  const handleSearch = React.useMemo(() => debounce((val) => getAutocomplete(val), 500), [])

  const handleInputChange = (e) => {
    const { value } = e.target
    setSearchValue(value)
    handleSearch(value)
  }

  const handleSubmit = (val) => {
    if (val === '') return

    setIsActive(false)
    document.activeElement.blur()
    router.push(`?query=${val || searchValue}`)

    addRecent(val || searchValue)
  }

  const action = (val) => {
    setSearchValue(val)
    handleSubmit(val)
  }

  const onClickItem = (e) => {
    const { innerText } = e.target
    action(innerText)
    document.body.style.overflow = 'auto'
  }

  const handleKeyDown = (e) => {
    const { key } = e
    let nextIndex = 0

    if (key === 'ArrowDown') {
      nextIndex = (focused + 1) % allItems.length
      setFocused(nextIndex)
    }

    if (key === 'ArrowUp') {
      nextIndex = (focused + allItems.length - 1) % allItems.length
      setFocused(nextIndex)
    }

    if (key === 'Enter') {
      const selectedItem = allItems[focused]
      if (selectedItem === undefined && searchValue === '') return
      selectedItem ? action(selectedItem) : action(searchValue)
      document.body.style.overflow = 'auto'
    }

    if (e.key === 'Escape') {
      setIsActive(false)
      e.target.blur()
      setSearchValue('')
      setFocused(-1)
      getAutocomplete('')
      document.body.style.overflow = 'auto'
    }
  }

  const handleFocus = () => {
    setIsActive(true)
    setFocused(-1)
    getAutocomplete(searchValue)
    document.body.style.overflow = 'hidden'
  }

  return (
    <div
      className={cn(styles.wrapper, isActive && styles.active)}
      role="search"
      tabIndex={1}
      onKeyDown={handleKeyDown}>
      <Input
        placeholder="Search Product"
        value={searchValue}
        onChange={handleInputChange}
        icon={<Search width={24} height={24} />}
        onFocus={handleFocus}
      />

      {isActive && recent.length > 0 && (
        <SearchList
          type="recent"
          items={recent}
          focused={focused}
          tabIndex={0}
          onClickItem={onClickItem}
          setRecent={setRecent}
        />
      )}
      {isActive && (
        <SearchList
          type="popular"
          items={list}
          focused={focused}
          tabIndex={0}
          onClickItem={onClickItem}
          recentLength={recent.length}
        />
      )}
    </div>
  )
}

export default SearchBar
