import * as React from 'react'
const SvgMenu = (props) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="1em"
    height="1em"
    fill="none"
    viewBox="0 0 24 24"
    {...props}>
    <path
      stroke="#1B153D"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}
      d="M1 12.333h11.901M1 5h22M1 19.667h22"
    />
  </svg>
)
export default SvgMenu
