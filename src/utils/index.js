function reverseString(str) {
  return str.split('').reverse().join('')
}

const utils = {
  formatCurrency(amount) {
    return new Intl.NumberFormat('sv-SE', {
      style: 'currency',
      currency: 'SEK',
      minimumFractionDigits: 2,
      maximumFractionDigits: 2
    }).format(amount / 100)
  },
  addToRecentSearches(search) {
    const recentSearches = JSON.parse(localStorage.getItem('recentSearches')) || []
    const checkIfExist = recentSearches.includes(search)
    if (checkIfExist) return
    const newRecentSearches = [search, ...recentSearches]
    localStorage.setItem('recentSearches', JSON.stringify(newRecentSearches))
  },
  getRecentSearches() {
    return JSON.parse(localStorage.getItem('recentSearches')) || []
  },
  removeItemFromRecentSearches(item) {
    const recentSearches = JSON.parse(localStorage.getItem('recentSearches')) || []
    const index = recentSearches.indexOf(item)
    recentSearches.splice(index, 1)
    localStorage.setItem('recentSearches', JSON.stringify(recentSearches))
  },
  clearRecentSearches() {
    localStorage.removeItem('recentSearches')
  },
  getProductImageUrl(image) {
    return image
      ? `https://d1ax460061ulao.cloudfront.net/140x150/${image[0]}/${image[1]}/${image}.jpg`
      : null
  }
}

export default utils
