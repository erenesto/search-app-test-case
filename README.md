# Search App Test Case

## Description

This is a test case for a search app. The app is a simple search app that allows users to search for a term and view the results. The app is built using Next.js and Css Modules.

## Features

- Search for a term
- View the autocomplete results
- View the recent searches
- Move on the list of results using the up and down arrow keys
- Delete a recent search
- Delete all recent searches
- View the results in a list

## Installation

1. Clone the repo
2. Run `yarn install` to install dependencies
3. Run `yarn dev` to start the app

## Tech Stack

- Next.js
- Css Modules
